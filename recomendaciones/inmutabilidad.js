//Se definen objetos con const para hacerlos inmutables
// no usar var o let
const obj = {
    a: 1
}

//Como mutar el objeto entonces?

const obj2 = {
    ...obj,  //spread operator, copia de obj
    c: 3
}


console.log(obj);
console.log(obj2);