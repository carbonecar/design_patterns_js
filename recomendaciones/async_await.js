const fn = async () => {
    return await new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Hola Mundo')
        }, 3000)
    })
}

let saludo = await = fn();
console.log(saludo);

// cuando se trabaja con promesas hay que usar async y await

