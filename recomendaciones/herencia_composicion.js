const saludar = (nombre) => console.log(`Hola soy ${nombre}`);
const despedir = (nombre) => console.log(`Adios soy ${nombre}`);
const dormir = (nombre) => console.log(`${nombre} esta durmiendo`);

const persona = {
    nombre: 'Juan',
    saludar: function () { saludar(this.nombre) }
}

class People {
    constructor(nombre) {
        this.nombre = nombre;
    }

    saludar() {
        saludar(this.nombre)
    }

    despedir() {
        despedir(this.nombre)
    }

    dormir() {
        dormir(this.nombre)
    }
}

class Robot {
    constructor(nombre) {
        this.nombre = nombre;
    }

    saludar() {
        saludar(this.nombre)
    }

    despedir() {
        despedir(this.nombre)
    }
}
