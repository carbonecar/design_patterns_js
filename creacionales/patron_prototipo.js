const perro = {
    raza: "Labrador",
    //si usamos una arrow function no podemos acceder a this
    ladrar: function () {
        console.log(`guau guau, soy un ${this.raza}`);
    }
}


const miPerro = Object.create(perro);

perro.ladrar();