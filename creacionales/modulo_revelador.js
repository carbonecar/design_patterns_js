//Aqui hay una parte publica y otra privada
const modulo = (() => {
    const x = {}
    return {
        a: () => console.log(x),
        b: (key, val) => x[key] = val
    }

})();

modulo.a();
modulo.b('nombre', 'juan');
modulo.a();

