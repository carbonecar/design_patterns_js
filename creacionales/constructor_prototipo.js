class MyClase {
    constructor(name) {
        this.name = name;
    }

    //metodo de prototipo. Si se modifica este metodo, se modifica tambien el metodo de la instancia
    metodo() {
        return this.name;
    }
}

const myClase = new MyClase('Juan');
console.log(myClase);