const persona = {
    name: "juan",
    networks: {
        twitter: false,
        linkedin: false
    }

}

// el objeto persona tiene un objeto dentro llamado networks. 
// al crear un nuevo objeto midu, este objeto se copia y se pasa como referencia.
// por lo tanto, si modificamos el objeto networks dentro de persona, tambien se modifica el objeto networks dentro de midu.
// si modificamos el objeto networks dentro de midu, no se modifica el objeto networks dentro de persona.

// ... spread operator permite copiar un objeto y sus propiedades y metodos
const midu = { ...persona }
midu.name = "javier";
midu.networks.twitter = true;
console.log(midu)
console.log(persona);




