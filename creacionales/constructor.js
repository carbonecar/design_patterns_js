class MiClase {
    constructor(nombre) {
        this.nombre = nombre;

        this.metodo = () => {
            return this.nombre;
        }
    }
}

// con new creamos una nueva instancia, se copian las propiedades y metodos
const instancia = new MiClase('Juan');
console.log(instancia);
