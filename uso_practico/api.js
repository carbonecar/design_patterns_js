const express = require('express');
const parser = require('body-parser');
const services = require('./services');
const handlify = require('./handlers');

const app = express();
const port = 3000;

const userHandlers = handlify('users')
const postsHandlers = handlify('posts')

//Patrones a usar: 
// 1. Singleton
// 2. Factory

app.use(parser.urlencoded({ extended: true }));

app.use(parser.json());

app.get('/', userHandlers(services).get);
app.post('/', userHandlers(services).post);
app.put('/:id', userHandlers(services).put);
app.delete('/', userHandlers(services).delete);


app.get('/posts', postsHandlers(services).get);


app.listen(port, () => console.log(`Example app listening on port ${port}!`));