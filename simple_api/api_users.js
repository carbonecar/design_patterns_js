//Esto no funciona porque fetch no esta en node
//Para que funcione hay que hacer npm install node-fetch
// import fetch from "node-fetch";
import fetch from "node-fetch";

const users = (() => {

    const recurso = 'https://jsonplaceholder.typicode.com/users'

    return {
        listar: async () => {
            return await fetch(recurso)
                .then(response => response.json())
        },
        crear: async (user) => {
            await fetch(recurso, {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
        }
    }
})();

users.listar().then(console.log);

