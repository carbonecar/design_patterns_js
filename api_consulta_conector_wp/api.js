const express = require('express');
const parser = require('body-parser');
const services = require('./services');
const handlers = require('./handlers');
const app = express();
const port = 3000;

//Patrones a usar: 
// 1. Singleton
// 2. Factory

app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

app.post('/login', handlers(services).post);


app.listen(port, () => console.log(`Example app listening on port ${port}!`));