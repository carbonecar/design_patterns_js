const axios = require('axios');

// patron singleton
const instance = axios.create({
    baseURL: 'https://apiqa.gwmp.navent.com',
    timeout: 1000,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
});

const adapter = {
    post: (url, body) => instance.post(url, body)

}

module.exports = adapter;
