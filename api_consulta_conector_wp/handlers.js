module.exports = ({ axios }) => ({
    post: async (req, res) => {
        const params = new URLSearchParams();
        params.append('username', 'user');
        params.append('password', 'password');
        const { data } = await axios.post('/login', params);
        return res.send(data);
    }
});
