
class User {
    constructor(name, email) {
        this.name = name;
        this.email = email;
    }
}

const user = new User()

const init = () => {
    user.on('login', userLoggedIn)
}

const userLoggedIn = () => {
    console.log('user logged in')
}

init()
