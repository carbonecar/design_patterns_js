let mixin = {
    saludar() {
        console.log(`Hola, soy ${this.nombre}`);
    },
    despedir() {
        console.log("chay,soy ${this.nombre}");
    }
}

class Persona {
    constructor(nombre) {
        this.nombre = nombre;
    }
}

Object.assign(Persona.prototype, mixin);

const Persona1 = new Persona("Juan");
Persona1.saludar();
