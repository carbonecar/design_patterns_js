//Esta es una funcion que devuelve otra funcion
const myFunction = (x) => {
    return () => {
        console.log(x);
    }
}

myFunction(3)();
