//Aca nos olvidamos de los parametros de la funcion

const f = (path, cb) => {
    //do something
    cb("hola");
}

//esta es la callback
const result = (resultado) => {
    console.log(resultado);
}

// se pasa la funcion como argumento directamente. Punteros a funcion
f("/paht", result);
