const auditProp = {
    createdAt: {
        default: new Date
    },
    updatedAt: {
        default: new Date
    },
    createBy: {
        type: Schema.Types.ObjectId,
        ref: "User"
    },
    updatedBy: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
}

const Model = defaultPropos => {
    return (name, props) => {
        const schema = moongose.schema({
            ...defaultPropos,
            ...props
        })
        return moongose.model(name, schema);
    }
}

export const withAudit = Model(auditProp);

/// en otro archivo

withAudit('User', {
    name: String,
    email: String
})