function suma(a, b) {
  return a + b;
}

function sumaCurrying(a) {
  return (b) => (c) => a + b + c;
}

console.log(suma(1, 2));
console.log(sumaCurrying(1)(2)(3));