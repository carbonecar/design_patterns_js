const users = [
    { edad: 20, nombre: 'Juan', appelido: "Perez" },
    { edad: 30, nombre: 'Maria', appelido: "Gonzalez" },
    { edad: 1, nombre: 'Pedro', appelido: "Martinez" },
]

const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x)

const pipe = (...fns) => x => fns.reduce((y, f) => f(y), x)


const traerPrimero = data =>
    format(createInfante(head(data.filter(user => user.edad < 2))))


const head = menores => menores[0]


const createInfante = primerMennor => ({
    nombreCompleto: `${primerMennor?.nombre} ${primerMennor?.appelido}`,
    edad: primerMennor?.edad
})

const format = infante =>
    `${infante.nombreCompleto} tiene ${infante.edad} años`

const filter = f => xs => xs.filter(f)

console.log(traerPrimero(users))

console.log(format(createInfante(head(users.filter(user => user.edad < 20)))))


const traerPrimerInfante = compose(
    format,
    createInfante,
    head,
    filter(x => x.edad < 2),
)

const traerPrimerInfantePipe = pipe(
    filter(x => x.edad < 2),
    head,
    createInfante,
    format,
)

traerPrimerInfante(users)
traerPrimerInfantePipe(users)